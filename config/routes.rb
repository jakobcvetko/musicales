Rails.application.routes.draw do
  root to: 'static#landing'

  get :home, controller: :static
  get :vokalna_glasba, controller: :static
  get :instrumentalna_glasba, controller: :static
  get :kompozicije, controller: :static
  get :seminarji, controller: :static
  get :izobrazevanja, controller: :static
  get :galerija, controller: :static

  get :cv, controller: :static
  get :sporocilo, controller: :static
  post :sporocilo, controller: :static, action: :handle_message

  get :narocilo, controller: :static
  post :narocilo, controller: :static, action: :handle_message_narocilo

  get :nakup, controller: :static
  post :nakup, controller: :static, action: :handle_message_nakup

  resources :posts, only: [:show]
  resources :albums, only: [:show]

  get :cookies, controller: :static, action: :cookies_info

  namespace :admin do
    root to: 'pages#index'

    resources :pages
    resources :posts do
      get :move_up
      get :move_down
    end
    resources :albums do
      get :move_up
      get :move_down
      resources :pictures, only: [:destroy]
    end
  end

  # Tinymce uploads
  post '/tinymce_assets' => 'tinymce_assets#create'
end

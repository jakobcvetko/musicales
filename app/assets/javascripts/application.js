// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui
// require turbolinks
//
//= require gallery/touch_touch
//
//= require jquery/jquery.easing-1.3
//= require jquery/jquery.elastislide
//= require jquery/jquery.tmpl.min

//= require cookies_eu

$(document).ready(function () {
  $('.touch').touchTouch();

  fade_in();
});

function fade_in() {
  $('.color-blink').animate({ color: "#FF0000" }, 500, function() {
    fade_out();
  });
}

function fade_out() {
  $('.color-blink').animate({ color: "#253183" }, 500, function() {
    fade_in();
  })
}


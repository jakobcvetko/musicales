class TinymceAssetsController < ApplicationController

  expose(:tinymce_asset, attributes: :tinymce_asset_attributes)

  def create
    if tinymce_asset.save
      render json: {
        image: {
          url: view_context.image_url(tinymce_asset.file.url)
        }
      }, content_type: "text/html"
    else
      render json: {
        errors: tinymce_asset.errors
      }, status: 400
    end
  end

  private
  def tinymce_asset_attributes
    params.permit(:file, :hint)
  end
end

class Admin::PostsController < Admin::BaseController

  expose(:post, attributes: :post_attributes)

  def index
  end

  def edit
  end

  def update
    if post.save
      redirect_to admin_posts_path
    else
      render :edit
    end
  end

  def create
    if post.save
      redirect_to admin_posts_path
    else
      render :edit
    end
  end

  def destroy
    post.destroy!
    redirect_to admin_posts_path
  end

  def move_up
    post.move_higher
    redirect_to admin_posts_path
  end

  def move_down
    post.move_lower
    redirect_to admin_posts_path
  end

  private
  def post_attributes
    params.require(:post).permit!
  end
end

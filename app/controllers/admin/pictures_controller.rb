class Admin::PicturesController < Admin::BaseController
  expose(:album)
  expose(:picture)

  def destroy
    picture.destroy!
    respond_to do |format|
      format.html { redirect_to edit_admin_album_path(album) }
      format.js
    end
  end

end

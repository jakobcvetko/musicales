class Admin::AlbumsController < Admin::BaseController

  expose(:albums) { Album.all }
  expose(:album, attributes: :album_attributes)

  def create
    if album.save
      redirect_to admin_albums_path
    else
      render :new
    end
  end

  def update
    if album.save
      redirect_to admin_albums_path
    else
      render :edit
    end
  end

  def destroy
    album.destroy
    redirect_to admin_albums_path
  end

  def move_up
    album.move_higher
    redirect_to admin_albums_path
  end

  def move_down
    album.move_lower
    redirect_to admin_albums_path
  end


  private
  def locale_names
    I18n.available_locales.map do |locale|
      [locale, :name].join('_').to_sym
    end
  end

  def locale_content
    I18n.available_locales.map do |locale|
      [locale, :content].join('_').to_sym
    end
  end

  def album_attributes
    permitted_params = [:position, image_urls: []] + locale_names + locale_content
    params.require(:album).permit(permitted_params)
  end
end

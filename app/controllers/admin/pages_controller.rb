class Admin::PagesController < Admin::BaseController

  expose(:page, attributes: :page_attributes)

  def index
  end

  def edit
  end

  def update
    if page.save
      redirect_to admin_pages_path
    else
      abort "Error!"
      render :edit
    end
  end

  private
  def page_attributes
    params.require(:page).permit(:id, :title, :content)
  end

end

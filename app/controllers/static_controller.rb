class StaticController < ApplicationController
  layout 'application'

  expose(:vocal_posts) { Post.vokalna.paginate(page: params[:page], per_page: per_page) }
  expose(:instrumental_posts) { Post.instrumentalna.paginate(page: params[:page], per_page: per_page) }

  def landing
    render layout: false
  end

  def home
    render text: Page.by_shortcode('index').content.html_safe, layout: true
  end

  def vokalna_glasba
  end

  def instrumentalna_glasba
  end

  def kompozicije
    render text: Page.by_shortcode('kompozicije').content.html_safe, layout: true
  end

  def seminarji
    render text: Page.by_shortcode('seminarji').content.html_safe, layout: true
  end

  def izobrazevanja
    render text: Page.by_shortcode('izobrazevanja').content.html_safe, layout: true
  end

  def cv
    render text: Page.by_shortcode('cv').content.html_safe, layout: true
  end

  def galerija
  end

  def sporocilo
  end

  def narocilo
  end

  def nakup
  end

  def cookies_info
  end

  def handle_message
    if valid_params?(params[:post])
      Rollbar.info("Message sent from web", post: params[:post])

      WebMailer.web_message(params[:post]).deliver
      WebMailer.web_message(params[:post], params[:post][:email]).deliver if params[:post][:copy]

      flash[:notice] = t("pisitenam.success")
      redirect_to :sporocilo
    else
      flash[:error] = t("pisitenam.fail")
      redirect_to :sporocilo
    end
  end

  def handle_message_narocilo
    if valid_params?(params[:post])
      Rollbar.info("Message sent from web", post: params[:post])

      WebMailer.web_message(params[:post]).deliver
      WebMailer.web_message(params[:post], params[:post][:email]).deliver if params[:post][:copy]

      flash[:notice] = t("narocilo.success")
      redirect_to :narocilo
    else
      flash[:error] = t("narocilo.fail")
      redirect_to :narocilo
    end
  end

  def handle_message_nakup
    if valid_params?(params[:post])
      Rollbar.info("Message sent from web", post: params[:post])

      WebMailer.web_message(params[:post]).deliver
      WebMailer.web_message(params[:post], params[:post][:email]).deliver if params[:post][:copy]

      flash[:notice] = t("narocilo.success")
      redirect_to :nakup
    else
      flash[:error] = t("narocilo.fail")
      redirect_to :nakup
    end
  end

  private

  def valid_params? attrs
    return false unless attrs
    return false unless attrs[:email].present?
    return false unless attrs[:message].present?
    true
  end

  def per_page
    (ENV['PER_PAGE'] || 10)
  end

end

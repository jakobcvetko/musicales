class Relative < ActiveRecord::Base

  ### Attributes
  #
  # post_id
  # relative_id
  #

  belongs_to :post
  belongs_to :relative, class_name: Post.to_s

  validates_presence_of :post_id
  validates_presence_of :relative_id

  after_create :reverse_duplicate
  after_destroy :reverse_destroy

  def reverse_duplicate
    Relative.find_or_create_by({
      post_id: relative_id,
      relative_id: post_id
    })
  end

  def reverse_destroy
    Relative.where({
      post_id: relative_id,
      relative_id: post_id
    }).destroy_all
  end

end

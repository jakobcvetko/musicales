class Picture < ActiveRecord::Base

  ### Attributes
  #
  # album_id
  # image
  # position
  #

  belongs_to :album
  attr_accessor :image_url
  mount_uploader :image, ImageUploader

  after_create :upload_image

  def upload_image
    self.remote_image_url = ['http:', image_url].join
    self.save
  end

end

class TinymceAsset < ActiveRecord::Base

  ### Attributes
  #
  # file
  # hint
  #

  mount_uploader :file, TinymceAssetUploader

end

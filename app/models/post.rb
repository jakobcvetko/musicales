class Post < ActiveRecord::Base

  ### Attributes
  #
  # orginal_naslov
  # naslov
  # avtor
  # zasedba
  # leto_nastanka : integer
  # lang
  # vrsta_izdaje
  # kataloska_st
  # ismn
  # opis : text
  # vokalna : boolean
  # instrumentalna : boolean
  # novo : boolean

  # besedilo
  # cena : text
  # file : string
  # file_avatar : string
  # images : text

  # youtube
  # mp3
  # free : boolean

  # position

  acts_as_list

  scope :vokalna, ->{ where vokalna: true }
  scope :instrumentalna, ->{ where instrumentalna: true }

  scope :newest, ->{ where(novo: true).order(created_at: :desc).first(2) }

  serialize :images, Array

  Post::MultilangAttrs = [ :naslov, :avtor, :besedilo, :zasedba,
    :lang, :vrsta_izdaje, :opis, :cena ]

  Post::MultilangAttrs.each do |attr_name|
    serialize attr_name, Hash

    define_method "localized_#{attr_name}" do |locale = I18n.locale|
      self.try(attr_name)[locale] || ""
    end

    I18n.available_locales.each do |locale|
      define_method "#{locale}_#{attr_name}" do
        self.try(attr_name)[locale]
      end

      define_method "#{locale}_#{attr_name}=" do |val|
        self.try(attr_name)[locale] = val
      end
    end
  end

  mount_uploader :file, PdfUploader
  mount_uploaders :images, PdfImageUploader
  mount_uploader :mp3, Mp3Uploader

  has_many :relatives
  has_many :related_posts, through: :relatives, source: :relative

  attr_accessor :related_post_ids

  validates_presence_of :orginal_naslov
  validates_presence_of :file

  before_validation :pdf_to_images, if: :file_changed?
  after_save :set_related_posts, unless: ->{ related_post_ids.nil? }

  def set_related_posts
    relatives.destroy_all

    related_post_ids.select(&:present?).each do |post_id|
      Relative.create({
        post: self,
        relative: Post.find(post_id)
      })
    end
  end

  def pdf_to_images
    puts "Starting"
    @pdf_file = MiniMagick::Image.open(self.file.path)
    self.images = @pdf_file.pages.each_with_index.map do |page, i|
      puts "Processing page #{i+1} ..."
      page.combine_options do |p|
        p.density(200)
        p.write(page.path+"#{i}.jpg")
      end

      puts "Uploading with carrierwave"
      File.open(page.path+"#{i}.jpg")
    end
  end

end

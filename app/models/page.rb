class Page < ActiveRecord::Base

  # Attributes
  #
  # title
  # shortcode
  # content:html
  # lang_code
  #

  scope :sl, ->{ where(lang_code: :sl) }
  scope :en, ->{ where(lang_code: :en) }

  def self.by_shortcode shortcode, lang=locale=I18n.locale
    Page.find_by(shortcode: shortcode, lang_code: lang) || Page.new
  end

  def content
    super || ""
  end

end

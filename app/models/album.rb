class Album < ActiveRecord::Base

  ### Attributes
  #
  # name
  # position
  # content : text
  #

  acts_as_list

  attr_accessor :image_urls

  has_many :pictures, dependent: :destroy

  # serialize :image_urls, Array
  serialize :name, Hash
  serialize :content, Hash

  after_save :create_pictures, if: :image_urls

  def create_pictures
    image_urls.each do |url|
      Picture.create({
        album: self,
        image_url: url
      })
    end
  end

  def localized_name locale=I18n.locale
    name[locale] || ""
  end

  def localized_content locale=I18n.locale
    content[locale] || ""
  end

  I18n.available_locales.each do |locale|
    define_method "#{locale}_name" do
      name[locale]
    end

    define_method "#{locale}_name=" do |val|
      name[locale] = val
    end

    define_method "#{locale}_content" do
      content[locale]
    end

    define_method "#{locale}_content=" do |val|
      content[locale] = val
    end
  end

  def image_presigned_post
    @image_presigned_post ||= S3_BUCKET.presigned_post(key: "uploads/#{SecureRandom.uuid}/${filename}", success_action_status: 201, acl: :public_read)
  end
end

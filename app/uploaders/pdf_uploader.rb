# encoding: utf-8

class PdfUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  def store_dir
    "uploads/posts/#{model.id}"
  end
end

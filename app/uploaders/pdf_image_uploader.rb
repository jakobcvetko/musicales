# encoding: utf-8

class PdfImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  def store_dir
    "uploads/posts/#{model.id}/images"
  end

  # process :resize_to_fit => [1000, 900]

  version :thumb do
    process :resize_to_fit => [300, 300]
  end
end

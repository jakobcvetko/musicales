class WebMailer < ActionMailer::Base
  default from: "info@musicales.si"

  def web_message(post, to=nil)
    @post = post
    mail(to: (to || admin_email), subject: 'Novo sporočilo iz strani www.musicales.si')
  end

  def admin_email
    ENV['ADMIN_EMAIL'] || "info@musicales.si"
  end
end

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Page.where(lang_code: nil).update_all(lang_code: :sl)

sl_pages = [
  { title: 'Domov', shortcode: 'index', content: '' },
  { title: 'Vokalna glasba', shortcode: 'vokal', content: '' },
  { title: 'Instrumentalna glasba', shortcode: 'instrumental', content: '' },
  { title: 'Kompozicije Aleš Makovac', shortcode: 'kompozicije', content: '' },
  { title: 'Seminarji', shortcode: 'seminarji', content: '' },
  { title: 'Aktualna izobraževanja', shortcode: 'izobrazevanja', content: '' },
  { title: 'Življenjepis', shortcode: 'cv', content: '' }
]

en_pages = [
  { title: 'Home', shortcode: 'index', content: '' },
  { title: 'Vokalna glasba', shortcode: 'vokal', content: '' },
  { title: 'Instrumentalna glasba', shortcode: 'instrumental', content: '' },
  { title: 'Kompozicije Aleš Makovac', shortcode: 'kompozicije', content: '' },
  { title: 'Seminarji', shortcode: 'seminarji', content: '' },
  { title: 'Aktualna izobraževanja', shortcode: 'izobrazevanja', content: '' },
  { title: 'Življenjepis', shortcode: 'cv', content: '' }
]

sl_pages.each do |page_params|
  page = Page.by_shortcode page_params[:shortcode], :sl
  if page.new_record?
    page.attributes = page_params.merge(lang_code: :sl)
    page.save
  end
end

en_pages.each do |page_params|
  page = Page.by_shortcode page_params[:shortcode], :en
  if page.new_record?
    page.attributes = page_params.merge(lang_code: :en)
    page.save
  end
end

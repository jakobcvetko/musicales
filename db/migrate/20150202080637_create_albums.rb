class CreateAlbums < ActiveRecord::Migration
  def change
    create_table :albums do |t|
      t.text :image_urls
      t.string :name
      t.integer :position

      t.timestamps
    end
  end
end

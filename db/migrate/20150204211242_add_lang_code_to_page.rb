class AddLangCodeToPage < ActiveRecord::Migration
  def change
    add_column :pages, :lang_code, :string
  end
end

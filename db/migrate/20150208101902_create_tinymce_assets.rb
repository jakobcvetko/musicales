class CreateTinymceAssets < ActiveRecord::Migration
  def change
    create_table :tinymce_assets do |t|
      t.string :file
      t.string :hint

      t.timestamps
    end
  end
end

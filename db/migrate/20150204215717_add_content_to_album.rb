class AddContentToAlbum < ActiveRecord::Migration
  def change
    add_column :albums, :content, :text
  end
end

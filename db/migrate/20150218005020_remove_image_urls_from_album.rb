class RemoveImageUrlsFromAlbum < ActiveRecord::Migration
  def change
    remove_column :albums, :image_urls, :text
  end
end

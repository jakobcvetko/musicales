class AddFieldsToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :free, :boolean, default: false
  end
end

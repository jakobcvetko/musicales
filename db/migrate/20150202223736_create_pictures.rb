class CreatePictures < ActiveRecord::Migration
  def change
    create_table :pictures do |t|
      t.string :image
      t.references :album, index: true
      t.integer :position

      t.timestamps
    end
  end
end

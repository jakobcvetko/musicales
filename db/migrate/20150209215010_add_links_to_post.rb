class AddLinksToPost < ActiveRecord::Migration
  def change
    add_column :posts, :youtube, :string
    add_column :posts, :mp3, :string
  end
end

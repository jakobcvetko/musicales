class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :naslov
      t.string :avtor
      t.string :zasedba
      t.integer :leto_nastanka
      t.string :lang
      t.string :vrsta_izdaje
      t.string :kataloska_st
      t.string :ismn
      t.text :opis
      t.boolean :vokalna
      t.boolean :instrumentalna
      t.boolean :novo

      t.timestamps
    end
  end
end

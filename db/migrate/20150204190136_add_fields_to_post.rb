class AddFieldsToPost < ActiveRecord::Migration
  def change
    add_column :posts, :besedilo, :string
    add_column :posts, :cena, :text
    add_column :posts, :file, :string
    add_column :posts, :file_avatar, :string
  end
end

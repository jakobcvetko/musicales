class CreateRelatives < ActiveRecord::Migration
  def change
    create_table :relatives do |t|
      t.references :post, index: true
      t.integer :relative_id

      t.timestamps
    end

    add_index :relatives, :relative_id
  end
end

# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150218005020) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "albums", force: true do |t|
    t.string   "name"
    t.integer  "position"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "content"
  end

  create_table "pages", force: true do |t|
    t.string   "title"
    t.text     "content"
    t.string   "shortcode"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "lang_code"
  end

  create_table "pictures", force: true do |t|
    t.string   "image"
    t.integer  "album_id"
    t.integer  "position"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "pictures", ["album_id"], name: "index_pictures_on_album_id", using: :btree

  create_table "posts", force: true do |t|
    t.string   "naslov"
    t.string   "avtor"
    t.string   "zasedba"
    t.integer  "leto_nastanka"
    t.string   "lang"
    t.string   "vrsta_izdaje"
    t.string   "kataloska_st"
    t.string   "ismn"
    t.text     "opis"
    t.boolean  "vokalna"
    t.boolean  "instrumentalna"
    t.boolean  "novo"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "besedilo"
    t.text     "cena"
    t.string   "file"
    t.string   "file_avatar"
    t.text     "images"
    t.string   "youtube"
    t.string   "mp3"
    t.boolean  "free",           default: false
    t.integer  "position"
    t.string   "orginal_naslov"
  end

  create_table "relatives", force: true do |t|
    t.integer  "post_id"
    t.integer  "relative_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "relatives", ["post_id"], name: "index_relatives_on_post_id", using: :btree
  add_index "relatives", ["relative_id"], name: "index_relatives_on_relative_id", using: :btree

  create_table "tinymce_assets", force: true do |t|
    t.string   "file"
    t.string   "hint"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end

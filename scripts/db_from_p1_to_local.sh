#!/usr/bin/env bash

heroku pgbackups:capture --app=musicales

curl -s -o tmp/latest.dump `heroku pgbackups:url --app=musicales`

pg_restore --verbose --clean --no-acl --no-owner -d musicales_dev tmp/latest.dump
